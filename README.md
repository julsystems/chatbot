## install dependencies
go to the project folder and run:

composer install

## DB
**to add the, in the project folder:**
cd db/
create a db with the name: chatbot
create a user with name rroot and pass 123456 otherwise change db config 
mysql -u dbuser -p chatbot < chatbot20200313.sql


## server
**laravel only localhost**
go to the project folder
php artisan serve
you access the project in your local machine only: http://127.0.0.1:8000

## if you want to run on your private server with a local domain 
cd /etc/apache2/sites-available/
touch chatbot.local.conf
add this to your file created above:

<VirtualHost *:80>

	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/html/chatbot/public
	ServerAlias chatbot.local


	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

	<Directory /var/www/html/chatbot/public>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Require all granted
        </Directory>


</VirtualHost>



**add to host file chatbot.local**
sudo vim /etc/hosts
127.0.1.1/private ip address in a foreign maching	chatbot.local


## to modify the sass and js source install dependencies with npm 
npm install


