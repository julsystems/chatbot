
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

function makeid(length) {
    var result           = '';
    var characters       = '123456789789456123';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }



 function callAPI(text) {
    
    var interactive = false;   
    var attachment = null;
    var callback = null;

    var postData = {
        driver: 'web',
        userId: userID,
        message: text
    };

    
    axios.post("/botman", postData).then(function (response) {
       
        var messages = response.data.messages || [];
        messages.forEach(function (msg) {

            //check if is login data
            if ( msg.text.indexOf( '|login|' ) >= 0 ) {

                let arrayMsg = msg.text.split('|login|');
                msg.text = arrayMsg[0];

                arrayMsg = arrayMsg[1].split('|');
                userLoginID = arrayMsg[0];
                userName = arrayMsg[1];                

                $('.container__login p').html('Login as: '+userName);
                $('.container__login').show();

            }
            
            renderBotResponse(msg.text);
        });
        if (callback) {
            callback(response.data);
        }
    });
}

function renderUserMessage( val ){
    const el = `
    <li class="ChatLog__entry ChatLog__entry_mine"><img src="/logo.png" class="ChatLog__avatar">
        <p class="ChatLog__message">${val}</p>
    </li>
    `;
    $( '.ChatLog' ).append(el);
}

function renderBotResponse(val){
    const el = `
    <li class="ChatLog__entry">
        <img src="/logo.png" class="ChatLog__avatar">
            <p class="ChatLog__message">
                ${val}
            </p>
    </li>	
    `;
    $( '.ChatLog' ).append(el);
}

function isLoginCommand( command ){

    const commandLowercase = command.toLowerCase();

    if ( commandLowercase.indexOf( 'current balance' ) >= 0 || commandLowercase.indexOf( 'deposit' ) >= 0 || commandLowercase.indexOf( 'withdraw' ) >= 0 ) {
        command = `${command} ${userLoginID}`;
    }

    return command;
}

var userID = makeid(3);
var userLoginID = 0;
var userName = '';




$('.ChatInput').focus();



$( '.ChatInput' ).keyup( function ( event ) {

    const userMessage = $( '.ChatInput' ).val();    
    
    if (  event.which === 13 ) {

        $( '.ChatInput' ).val('');

        renderUserMessage( userMessage );
        
        callAPI( isLoginCommand(userMessage) );

        $("html, body").animate({ scrollTop: $(document).height() }, 1000);
        const p  = $( $('ul.ChatLog .ChatLog__entry_mine')[$('ul.ChatLog .ChatLog__entry_mine').length-1] ).find('p')[0];
        let lastMessage = $(p).html().trim();
        lastMessage = lastMessage.replace(/\n/g, '');
        lastMessage = lastMessage.replace(/<\!\-\-\-\->/g, '');
        lastMessage = lastMessage.trim();
        
        if (  lastMessage.indexOf('signup') >= 0  ) {

            let arrayMessage = lastMessage.split(" ");
            
            if ( arrayMessage.length === 5 ) {

                $(p).html( `signup ${arrayMessage[1]} ${arrayMessage[2]} ${arrayMessage[3]} *****` );

            }
 
        } else if (  lastMessage.indexOf('login') >= 0  ) {

            let arrayMessage = lastMessage.split(" ");
            
            if ( arrayMessage.length === 3 ) {

                $(p).html( `login ${arrayMessage[1]} *****` );

            }

        }



    }

} );

