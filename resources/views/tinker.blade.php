<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Jobsity Bot</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
    <link href="/css/app.css" rel="stylesheet" type="text/css">

</head>
<body>
<div class="container">
    <div class="container__login">
        <p>login as: julio</p>
    </div>
    <div id="app" class="content"><div><div class="arrow"></div> 
    <ul class="ChatLog">        
	</ul> 
	<input type="text" placeholder="say hi to the bot!" class="ChatInput"></div></div>
</div>

<script src="/js/app.js"></script>
</body>
</html>