<?php
use App\Http\Controllers\BotManController;
use App\Http\Controllers\pfc_balancesController;
use App\Http\Controllers\pfc_bankAccountsController;
use App\Http\Controllers\pfc_incomesController;
use App\Http\Controllers\pfc_outgoinsController;
use App\Http\Controllers\pfc_usersController;


$botman = resolve('botman');

$botman->hears('hi', function ($bot) {
    $bot->reply('hello!');    
    $bot->reply('how can I help you?');
});

$botman->hears('How\sare\sya\?', function ($bot) {    
    $bot->reply('fine!');
});

$botman->hears('what\stime\sis\sit\?', function( $bot ){

    $bot->reply('it\'s: '. date('h:m'));

});

$botman->hears("Hello\sI'm\s{name}", function ( $bot, $name ) {

    $bot->reply('Hi '.trim($name));

});


$botman->hears('signup\s{fname}\s{lname}\s{nickname}\s{pass}', function ( $bot, $fname, $lname, $nickname, $pass) {


    $userController = new pfc_usersController();    
    
    $rs = $userController->store( $fname, $lname, $nickname, $pass, 'defaul@example.com'  );

    if ( $rs == 'ok' ) {

        $bot->reply("$fname, Thanks for signing up!");
        $bot->reply("Now you can login using this command: login username pass");

    } else {

        $bot->reply("Internal error, please try later!".$rs);

    }

    

});

$botman->hears('login\s{nickname}\s{pass}', function ( $bot, $nickname, $pass ) {

    
    $userController = new pfc_usersController();    
    
    $userInfo = $userController->getbyNicknameNPass($nickname, $pass );

    if( $userInfo != 'failed' ) {

        $bot->reply("Hello $userInfo[userFirstName]|login|$userInfo[userID]|$userInfo[userFirstName]");

    } else {

        $bot->reply('Invalid user or password! ');

    }

    

});


$botman->hears('current\sbalance{userID}', function ( $bot, $userID ) {

    $userID  = (int) $userID ;

    if( $userID === 0 ) {
        $bot->reply('Please login first!');
    } else {

        $Balance = new pfc_balancesController();
        $Account = new pfc_bankAccountsController();

        $accountDetails = $Account->getAccountByUserID($userID);
        $currentBalance = $Balance->getBalancebyUserID( $userID );    
            

        $bot->reply('Your current balance is: '.$currentBalance ." ".$accountDetails->bat_Currency);
    }

    

});

$botman->hears('deposit\s{amount}\s{userID}', function ( $bot, $amount, $userID  ) {

    $userID  = (int) $userID ;

    if( $userID === 0 ) {

        $bot->reply('Please login first!');

    } else { 

        $Income = new pfc_incomesController();
        $Account = new pfc_bankAccountsController();

        $accountDetails = $Account->getAccountByUserID($userID);
        $Income->create( $userID,$accountDetails->bat_ID, $amount, "chatbot", "it was deposited from chatbot" );

        $bot->reply('You have deposited: '.$amount);
        $bot->reply('You can check with this commnad: current balance');
    }

});

$botman->hears('withdraw\s{amount}\s{userID}', function ( $bot, $amount, $userID  ) {

    $userID  = (int) $userID ;

    if( $userID === 0 ) {

        $bot->reply('Please login first!');

    } else { 

        $Outgoing = new pfc_outgoinsController();
        $Account = new pfc_bankAccountsController();

        $accountDetails = $Account->getAccountByUserID($userID);
        $status = $Outgoing->create( $userID,$accountDetails->bat_ID, $amount, "chatbot", "it was withdrawn from chatbot" );


        if ( $status == "false" ) {

            $bot->reply('insufficient balance!');
            $bot->reply('plese check your balance with: current balance');
        

        } else {

            $bot->reply('You have withdrawn: '.$amount ." ". $accountDetails->bat_Currency);
            $bot->reply('You can check with this commnad: current balance');

        }

        
    }

});


/*$botman->hears('', function ( $bot ) {

    $bot->reply('');

});*/

$botman->fallback(function($bot) {
    $bot->reply('Sorry, I did not understand these commands. Try with: hi');
});


function sendInfo( $url, $data, $count  ){
    /*static protected $method = [
        'post' => CURLOPT_POST,
        'get' => CURLOPT_HTTPGET,
        'put' => CURLOPT_PUT,
            //TODO: DELETE
    ]*/

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch,CURLOPT_HEADER, false); 
    curl_setopt($ch, CURLOPT_POST, $count);
    curl_setopt($ch, CURLOPT_POSTFIELDS,    $data);
    

    $rs = curl_exec($ch);
    
    curl_close ($ch);
    
    return $rs;

}

$botman->hears('Start conversation', BotManController::class.'@startConversation');
