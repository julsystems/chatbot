<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class pfc_outgoins extends Model
{
    
    const CREATED_AT = 'otg_CreatedAt';
    const UPDATED_AT = 'otg_UpdatedAt';
    protected $primaryKey = 'otg_ID';

    protected $fillable = [
        'otg_UserID', 'otg_BankAccountID', 'otg_Amount','otg_Type','otg_Desc'
    ];
}
