<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class pfc_balances extends Model
{
    
    const CREATED_AT = 'blc_CreatedAt';
    const UPDATED_AT = 'blc_UpdatedAt';
    protected $primaryKey = 'blc_ID';

    protected $fillable = [
        'blc_UserID', 'blc_BankAccountID', 'blc_Amount'
    ];

    protected $hidden = [
        '',
    ];

}
