<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class pfc_incomes extends Model
{
    
    const CREATED_AT = 'inc_CreatedAt';
    const UPDATED_AT = 'inc_UpdatedAt';
    protected $primaryKey = 'inc_ID';

    protected $fillable = [
        'inc_UserID', 'inc_BankAccountID', 'inc_Amount','inc_Type','inc_Desc'
    ];
}
