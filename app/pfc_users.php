<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class pfc_users extends Model
{
    
    const CREATED_AT = 'usr_CreatedAt';
    const UPDATED_AT = 'usr_UpdatedAt';
    protected $primaryKey = 'usr_ID';

    protected $fillable = [
        'usr_FirstName', 'usr_LastName', 'usr_NickName','usr_Email','usr_Password'
    ];

    protected $hidden = [
        '',
    ];

}
