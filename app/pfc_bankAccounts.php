<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pfc_bankAccounts extends Model
{
    
    protected $table = 'pfc_bankAccounts';
    const CREATED_AT = 'bat_CreatedAt';
    const UPDATED_AT = 'bat_UpdatedAt';
    protected $primaryKey = 'bat_ID';


    protected $fillable = [
        'bat_UserID', 'bat_Number', 'bat_Type','bat_Bank','bat_Currency'
    ];
}
