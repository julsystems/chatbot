<?php

namespace App\Http\Controllers;

use App\pfc_incomes;
use Illuminate\Http\Request;
use App\Http\Controllers\pfc_balancesController;





class pfc_incomesController extends Controller
{
  
    public function create( $userID,$accountID, $amount, $type, $desc ){

      $Income = new pfc_incomes([
        'inc_UserID' => $userID,
        'inc_BankAccountID' => $accountID,
        'inc_Amount' => $amount,
        'inc_Type' => $type,
        'inc_Desc' => $desc,
      ]);

      $Income->save();

      $Balance = new pfc_balancesController();
      $currentBalance = $Balance->getBalancebyUserID( $userID );    
      $Balance->updateBalance( $userID,  ( (float) $currentBalance + $amount ) );
    

    }

       
}
