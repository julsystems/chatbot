<?php

namespace App\Http\Controllers;

use App\pfc_outgoins;
use Illuminate\Http\Request;
use App\Http\Controllers\pfc_balancesController;





class pfc_outgoinsController extends Controller
{
  
    public function create( $userID,$accountID, $amount, $type, $desc ){

      $Outgoing = new pfc_outgoins([
        'otg_UserID' => $userID,
        'otg_BankAccountID' => $accountID,
        'otg_Amount' => $amount,
        'otg_Type' => $type,
        'otg_Desc' => $desc,
      ]);      

      $Balance = new pfc_balancesController();
      $currentBalance = $Balance->getBalancebyUserID( $userID );  
      $currentBalance = str_replace(",","", $currentBalance);
      

      if ( $currentBalance >= $amount ) {

        $Outgoing->save();  

        $Balance->updateBalance( $userID,  ( (float) $currentBalance - $amount ) );

        return "true";

      } else {

        return "false";

      }

      
    

    }

       
}
