<?php

namespace App\Http\Controllers;

use App\pfc_users;
use App\Http\Controllers\pfc_bankAccountsController;
use Illuminate\Http\Request;





class pfc_usersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    public function get(){
       // return response()->json(pfc_users::get(),200);
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( $usr_FirstName, $usr_LastName, $usr_NickName, $usr_Password, $usr_Email  )
    {


        $User = new pfc_users([
            'usr_FirstName' => $usr_FirstName,
            'usr_LastName' => $usr_LastName,
            'usr_NickName' => $usr_NickName,
            'usr_Password' => md5($usr_Password),
            'usr_Email' =>  $usr_Email
        ]);

        $res = $User->save();

        //creating account
        $userID = $this->localGetbyNicknameNPass( $usr_NickName, $usr_Password );

        $Account = new pfc_bankAccountsController();
        $accountID = $Account->create($userID);
        

        
        return 'ok';
    }

    public function getbyNicknameNPass($usr_NickName, $usr_Password ) {

        $nickname = $usr_NickName;
        $pass = md5($usr_Password);

        //return response()->json($pass, 200);
        $data = pfc_users::where('usr_NickName', $nickname)->where('usr_Password', $pass)->get();
        

        foreach( $data as $value ) {
           $userData = $value;
        }
        
        if( count( $data ) > 0 ) {

            return ['userID'=>$userData->usr_ID,
            'userFirstName'=>$userData->usr_FirstName,
            ];

        }

       
        return 'failed';
        

    }


    public function localGetbyNicknameNPass( $usr_NickName, $usr_Password ) {

        $nickname = $usr_NickName;
        $pass = md5($usr_Password);

        
        $data = pfc_users::where('usr_NickName', $nickname)->where('usr_Password', $pass)->get();
        
        
        
        foreach( $data as $value ) {
           $userData = $value;
        }
        
       
       
        return $userData->usr_ID;
        

    }

   
}
