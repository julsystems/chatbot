<?php

namespace App\Http\Controllers;

use App\pfc_bankAccounts;
use App\Http\Controllers\pfc_balancesController;
use Illuminate\Http\Request;





class pfc_bankAccountsController extends Controller
{
  
    public function create( $userID ){

      $Account = new pfc_bankAccounts([
        'bat_UserID' => $userID,
        'bat_Number' => rand(123456,456789),
        'bat_Type' => 'Checking',
        'bat_Bank' => 'Scotia Bank',
        'bat_Currency' => 'USD'
    ]);

    $res = $Account->save();
    $accountID = $this->getByUserID( $userID );

    $BalanceController = new pfc_balancesController();
    $BalanceController->create($userID, $accountID );

    return $accountID;
    

    }

    public function getByUserID( $userID ){

      $data = pfc_bankAccounts::where('bat_UserID', $userID)->get();
        
        
        
        foreach( $data as $value ) {
          $accountData = $value;
        }
        
       
       
        return $accountData->bat_ID;

        
    }

    public function getAccountByUserID( $userID ){

      $data = pfc_bankAccounts::where('bat_UserID', $userID)->get();
        
        
        
        foreach( $data as $value ) {
           $accountData = $value;
        }
        
       
       
        return $accountData;

    }
   
}
