<?php

namespace App\Http\Controllers;

use App\pfc_balances;
use Illuminate\Http\Request;





class pfc_balancesController extends Controller
{
  
    public function create( $userID,$accountID ){

      $Balance = new pfc_balances([
        'blc_UserID' => $userID,
        'blc_BankAccountID' => $accountID,
        'blc_Amount' => '0.0',
      ]);

      $Balance->save();
    

    }

    public function getByUserID( $userID ){

      $data = pfc_balances::where('blc_UserID', $userID)->get();
        
        
        
        foreach( $data as $value ) {
           $bankData = $value;
        }
        
       
       
        return $bankData->blc_ID;

    }

    public function getBalancebyUserID( $userID ){

      $data = pfc_balances::where('blc_UserID', $userID)->get();
        
        
        
        foreach( $data as $value ) {
           $bankData = $value;
        }
        
       
       
        return number_format($bankData->blc_Amount,2);

    }

    public function updateBalance( $userID,  $newBalance ) {

      pfc_balances::where('blc_UserID', $userID  )->update(['blc_Amount'=> $newBalance ]);

    }
   
}
