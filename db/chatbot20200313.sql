-- MySQL dump 10.13  Distrib 8.0.19, for Linux (x86_64)
--
-- Host: localhost    Database: chatbot
-- ------------------------------------------------------
-- Server version	8.0.19-0ubuntu0.19.10.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pfc_balances`
--

DROP TABLE IF EXISTS `pfc_balances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pfc_balances` (
  `blc_ID` int NOT NULL AUTO_INCREMENT,
  `blc_UserID` int NOT NULL,
  `blc_BankAccountID` int NOT NULL,
  `blc_Amount` decimal(65,15) DEFAULT NULL,
  `blc_CreatedAt` datetime DEFAULT NULL,
  `blc_UpdatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`blc_ID`),
  KEY `balances_userID_idx` (`blc_UserID`),
  KEY `fk_pfc_balances_banaccountid_idx` (`blc_BankAccountID`),
  CONSTRAINT `balances_userID` FOREIGN KEY (`blc_UserID`) REFERENCES `pfc_users` (`usr_ID`),
  CONSTRAINT `fk_pfc_balances_banaccountid` FOREIGN KEY (`blc_BankAccountID`) REFERENCES `pfc_bankAccounts` (`bat_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pfc_balances`
--

LOCK TABLES `pfc_balances` WRITE;
/*!40000 ALTER TABLE `pfc_balances` DISABLE KEYS */;
/*!40000 ALTER TABLE `pfc_balances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pfc_bankAccounts`
--

DROP TABLE IF EXISTS `pfc_bankAccounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pfc_bankAccounts` (
  `bat_ID` int NOT NULL AUTO_INCREMENT,
  `bat_UserID` int NOT NULL,
  `bat_Number` varchar(255) NOT NULL,
  `bat_Type` varchar(255) DEFAULT NULL,
  `bat_Bank` varchar(255) DEFAULT NULL,
  `bat_CreatedAt` datetime DEFAULT NULL,
  `bat_UpdatedAt` datetime DEFAULT NULL,
  `bat_Currency` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`bat_ID`),
  KEY `banck_aacount_users_id_idx` (`bat_UserID`),
  CONSTRAINT `banck_aacount_users_id` FOREIGN KEY (`bat_UserID`) REFERENCES `pfc_users` (`usr_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pfc_bankAccounts`
--

LOCK TABLES `pfc_bankAccounts` WRITE;
/*!40000 ALTER TABLE `pfc_bankAccounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `pfc_bankAccounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pfc_incomes`
--

DROP TABLE IF EXISTS `pfc_incomes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pfc_incomes` (
  `inc_ID` int NOT NULL AUTO_INCREMENT,
  `inc_UserID` int NOT NULL,
  `inc_BankAccountID` int NOT NULL,
  `inc_Amount` decimal(65,15) NOT NULL,
  `inc_Type` varchar(128) DEFAULT NULL,
  `inc_Desc` varchar(255) NOT NULL,
  `inc_CreatedAt` datetime DEFAULT NULL,
  `inc_UpdatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`inc_ID`),
  KEY `incomes_users_id_idx` (`inc_UserID`),
  KEY `incomes_bankaccountid_idx` (`inc_BankAccountID`),
  CONSTRAINT `incomes_bankaccountid` FOREIGN KEY (`inc_BankAccountID`) REFERENCES `pfc_bankAccounts` (`bat_ID`),
  CONSTRAINT `incomes_users_id` FOREIGN KEY (`inc_UserID`) REFERENCES `pfc_users` (`usr_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pfc_incomes`
--

LOCK TABLES `pfc_incomes` WRITE;
/*!40000 ALTER TABLE `pfc_incomes` DISABLE KEYS */;
/*!40000 ALTER TABLE `pfc_incomes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pfc_outgoins`
--

DROP TABLE IF EXISTS `pfc_outgoins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pfc_outgoins` (
  `otg_ID` int NOT NULL AUTO_INCREMENT,
  `otg_UserID` int NOT NULL,
  `otg_BankAccountID` int NOT NULL,
  `otg_Amount` decimal(65,15) NOT NULL,
  `otg_Type` varchar(128) DEFAULT NULL,
  `otg_Desc` varchar(255) NOT NULL,
  `otg_CreatedAt` datetime DEFAULT NULL,
  `otg_UpdatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`otg_ID`),
  KEY `outgoings_users_id_idx` (`otg_UserID`),
  KEY `outgoings_bankAccount_ID_idx` (`otg_BankAccountID`),
  CONSTRAINT `outgoings_bankAccount_ID` FOREIGN KEY (`otg_BankAccountID`) REFERENCES `pfc_bankAccounts` (`bat_ID`),
  CONSTRAINT `outgoings_users_id` FOREIGN KEY (`otg_UserID`) REFERENCES `pfc_users` (`usr_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pfc_outgoins`
--

LOCK TABLES `pfc_outgoins` WRITE;
/*!40000 ALTER TABLE `pfc_outgoins` DISABLE KEYS */;
/*!40000 ALTER TABLE `pfc_outgoins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pfc_users`
--

DROP TABLE IF EXISTS `pfc_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pfc_users` (
  `usr_ID` int NOT NULL AUTO_INCREMENT,
  `usr_FirstName` varchar(255) DEFAULT NULL,
  `usr_LastName` varchar(255) DEFAULT NULL,
  `usr_NickName` varchar(255) NOT NULL,
  `usr_Password` varchar(255) DEFAULT NULL,
  `usr_Avatar` varchar(255) DEFAULT NULL,
  `usr_Email` varchar(255) NOT NULL,
  `usr_CreatedAt` datetime DEFAULT NULL,
  `usr_UpdatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`usr_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pfc_users`
--

LOCK TABLES `pfc_users` WRITE;
/*!40000 ALTER TABLE `pfc_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `pfc_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-13 10:43:28
